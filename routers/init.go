package routers

import (
	"git.rappet.de/rappet/go-url-shortener/modules/setting"
)

// GlobalInit is for global configuration reload-able.
func GlobalInit() {
	setting.NewContext()
}
