package routes

import (
	"git.rappet.de/rappet/go-url-shortener/modules/templates"

	macaron "gopkg.in/macaron.v1"
)

func NewMacaron() *macaron.Macaron {
	m := macaron.New()

	m.Use(templates.HTMLRenderer())

	return m
}

func RegisterRoutes(m *macaron.Macaron) {
	m.Get("/", func(ctx *macaron.Context) {
		ctx.Data["Title"] = "Home"
		ctx.HTML(200, "home")
	})
}
