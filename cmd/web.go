package cmd

import (
	"git.rappet.de/rappet/go-url-shortener/routers"
	"git.rappet.de/rappet/go-url-shortener/routers/routes"

	"github.com/urfave/cli"
)

var CmdWeb = cli.Command{
	Name: "web",
	Usage: "Start URL shortener web server",
	Action: runWeb,
	Flags: []cli.Flag{
		cli.StringFlag{
			Name: "port, p",
			Value: "8080",
			Usage: "Temporary port number to prevent conflict",
		},
	},
}

func runWeb(ctx *cli.Context) error {
	routers.GlobalInit()

	m := routes.NewMacaron();
	routes.RegisterRoutes(m);

	m.Run()

	return nil
}
