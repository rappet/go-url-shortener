package templates

import (
	"git.rappet.de/rappet/go-url-shortener/modules/setting"	
	
	"html/template"
)

// NewFuncMap returns functions for injecting to templates
func NewFuncMap() []template.FuncMap {
	return []template.FuncMap{map[string]interface{}{
		"AppName": func() string {
			return setting.AppName
		},
		"AppVer": func() string {
			return setting.AppVer
		},
	}}
}
