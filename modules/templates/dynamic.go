package templates

import (
	"html/template"

	"gopkg.in/macaron.v1"
)

var (
	templates = template.New("")
)

// HTMLRenderer implements the macaron handler for serving HTML templates.
func HTMLRenderer() macaron.Handler {
	return macaron.Renderer(macaron.RenderOptions{
		Directory: "templates",
		Funcs: NewFuncMap(),
	})
}
