package main

import (
	"os"
	"log"

	"git.rappet.de/rappet/go-url-shortener/modules/setting"
	"git.rappet.de/rappet/go-url-shortener/cmd"

	"github.com/urfave/cli"
)

var Version = "0.0.1"

func init() {
	setting.AppVer = Version
}

func main() {
	app := cli.NewApp()
	app.Name = "Go-URL-Shortener"
	app.Usage = "A simple URL shortener written in go."
	app.Description = "By default, a server will created on port 8080."
	app.Version = "0.0.1"
	app.Action = cmd.CmdWeb.Action
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(4, "Failed to run app with %s: %v", os.Args, err)
	}
}
